# Gazette FFDN

Il a été décidé à l'AG2016 de demander aux assos membres de la fédé de
fournir un compte rendu de leurs activités une fois par mois ?
trimestre ?. Et de "touiller" en cas de non réponse.

Une gazette des nouvelles des membres de la FFDN et futurs membres.

Cette outil permet de collecter les actualités des FAI et d'envoyer une gazette à ces mêmes FAI

Voir notre pad de travail ici : https://pad.ilico.org/p/gazette-ffdn

# Install

```bash
  $ virtualenv -p python3 venv && source venv/bin/activate
  $ pip install -r requirements.txt
```

# Configuration

```bash
  $ python manage.py migrate
  $ python manage.py createsuperuser
```

